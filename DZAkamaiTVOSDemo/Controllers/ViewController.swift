//
//  ViewController.swift
//  DZAkamaiTVOSDemo
//
//  Created by Vinu Varghese on 22/06/20.
//  Copyright © 2020 Datazoom. All rights reserved.
//

import UIKit
import TVUIKit

class ViewController: UIViewController {
    
    private static let UNCHECKED_IMG = UIImage(named: "unCheckedBox")
    private static let CHECKED_IMG = UIImage(named: "checkedBox")
    private static let POSTER_IMG = UIImage(named: "posterImage")

    private static let VOD = "https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8"
    private static let LIVE = "https://liveprodeuwest.akamaized.net/eu1/Channel-EUTVqvs-AWS-ireland-1/Source-EUTVqvs-1000-1_live.m3u8"
    private var appEnv = "Dev"
    private var appConfigId = "43f02187-2823-4d40-9178-170a31899f35"
    private var selectedUrl = VOD
    private var isVOD = true
    private var selectedAdProvider = String()
    private var includeAds: Bool!

    @IBOutlet weak var txtConfigId: UITextField!
    @IBOutlet weak var tvPosterImage: TVPosterView!
    @IBOutlet var envBtns: [UIButton]!
    @IBOutlet var btnOutlet_wantAds: UIButton!
    
    @IBAction func actionEnvSelect(_ sender: UIButton) {
        envBtns.forEach({ btn in
            btn.isSelected = false
        })
        sender.isSelected = true
        switch (sender.currentTitle) {
        case "Dev":
            appEnv = "Dev"
            break
        case "Staging":
            appEnv = "Staging"
            break
        case "Prod":
            appEnv = "Prod"
            break
        default:
            break
        }
    }
    
    @IBAction func btnActn_wantAds(_ sender: UIButton) {
        if(btnOutlet_wantAds.currentImage == ViewController.UNCHECKED_IMG) {
            btnOutlet_wantAds.setImage(ViewController.CHECKED_IMG, for: .normal)
            includeAds = true
        } else {
            btnOutlet_wantAds.setImage(ViewController.UNCHECKED_IMG, for: .normal)
            includeAds = false
        }
    }

    @IBAction func actionPlayLive(_ sender: TVPosterView) {
        self.selectedUrl = ViewController.LIVE
        self.isVOD = false
        if(includeAds == true) {
            showAddSelectorOptions()
        } else {
            self.selectedAdProvider = ""
            self.performSegue(withIdentifier: "showVideo", sender: self)
        }
    }
    
    @IBAction func actionPlay(_ sender: TVPosterView) {
        self.selectedUrl = ViewController.VOD
        self.isVOD = true
        if(includeAds == true) {
            showAddSelectorOptions()
        } else {
            self.selectedAdProvider = ""
            self.performSegue(withIdentifier: "showVideo", sender: self)
        }
    }
    
    func showAddSelectorOptions() {
        let alert = UIAlertController(title: "Datazoom", message: "Please select ad provider", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Google IMA", style: .default, handler: { [self] action in
            self.selectedAdProvider = "IMA"
            self.performSegue(withIdentifier: "showVideo", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "Google IMA Skippable", style: .default, handler: { [self] action in
            self.selectedAdProvider = "IMA-Skip"
            self.performSegue(withIdentifier: "showVideo", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "Google IMA Error", style: .default, handler: { [self] action in
            self.selectedAdProvider = "IMA-Error"
            self.performSegue(withIdentifier: "showVideo", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "Freewheel", style: .default, handler: { [self] action in
            self.selectedAdProvider = "Freewheel"
            self.performSegue(withIdentifier: "showVideo", sender: self)
        }))
//        alert.addAction(UIAlertAction(title: "Freewheel Error", style: .default, handler: { [self] action in
//            self.selectedAdProvider = "Freewheel-Error"
//            self.performSegue(withIdentifier: "showVideo", sender: self)
//        }))
        self.present(alert, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtConfigId.text = appConfigId
        tvPosterImage.contentSize = CGSize(width: 50, height: 50)
        tvPosterImage.image = ViewController.POSTER_IMG
        envBtns[0].isSelected = true
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showVideo" {
            let videoVc = segue.destination as! VideoViewController
            videoVc.appEnv = self.appEnv
            videoVc.configId = self.txtConfigId.text
            videoVc.selectedUrl = self.selectedUrl
            videoVc.isVod = self.isVOD
            videoVc.selectedAdProvider = self.selectedAdProvider
        }
    }

}
