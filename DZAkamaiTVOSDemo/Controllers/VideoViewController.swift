//
//  VideoViewController.swift
//  DZAkamaiTVOSDemo
//
//  Created by Vinu Varghese on 29/06/20.
//  Copyright © 2020 Datazoom. All rights reserved.
//

import UIKit
import AmpCore
import AmpIMA
import AmpFreewheel
import AVKit
import DZAkamaiTVOSCollector
import DzTvOSBase
import DZAkamaiTVOSIMACollector
import DZAkamaiTVOSFreewheelCollector

class VideoViewController: UIViewController {
    private let ampLicense = "AwECs0Ug2tYDP9TBnw4JK84rqQhHpukHYS5EA4Xn96eecCGIT2WTtX0Ur541fqZa5sRfpkqKX2NoDvz04gwkDzutD9Jh3JaY179WOfXLFzneVHOXEUC+3XnwjrISax4+qlohAAkl39ZE+kTRiRkdWHAKeSfs5lUp6CFqiNK27pPCZbLbyI5/+V6Vmmusp1fXoE7lPkOm8JV52+t0aoIIfKeF3+WdozIaHnzynOjA8OrlIw=="

    var ampPlayer: AmpPlayer?
    var avplayerController: AVPlayerViewController?
    var ima: AmpIMAManager!

    var appEnv: String?
    var configId: String?
    var selectedUrl: String?
    var isVod: Bool?
    
    var selectedAdProvider: String?
    var ampFreewheel: AmpFreewheelManager!

    override func viewDidLoad() {
        super.viewDidLoad()
        print("Using appenv \(appEnv) and Config \(configId)")
        var dzEnv = DZEnv.DEV
        switch appEnv {
        case "Dev":
            dzEnv = .DEV
            break
        case "Staging":
            dzEnv = .STAGING
            break
        case "Prod":
            dzEnv = .PROD
            break
        default:
            break
        }
        
        // Do any additional setup after loading the view.
        self.avplayerController = AVPlayerViewController()
        ampPlayer = AmpPlayer(parentViewController: self, playerController: self.avplayerController!)
        ima = AmpIMAManager(ampPlayer: ampPlayer!, videoView: self.view)
        
        let networkId = 42015
        let serverUrl = "https://demo.v.fwmrm.net"
        self.ampFreewheel = AmpFreewheelManager(ampPlayer: self.ampPlayer!, adView: self.view, networkId: networkId, playerViewController: self.avplayerController!)
        
        let videoAssetIdStr = "DZTEST\(!isVod! ? "LIVE" : "VOD")ASSET"
        let videoAssetId = VideoAssetId(id: videoAssetIdStr, duration: 0)
        
        var type = ( isVod ?? true ) ? "VOD" : "Live"
        ampPlayer?.title = "Demo \( type ) video"
        ampPlayer?.autoplay = true
        ampPlayer?.logsEnabled = true
        ampPlayer?.setLicense(ampLicense)
        ampPlayer?.registerObserver(self)
        DZAkamaiTvOsCollectorInstance.shared.initialize(dzEnv, self.configId!, ampPlayer!, onCompletion: {})
        DZAkamaiTvOsCollectorInstance.shared.setCustomMetaData([
            "userId": "customUserId",
            "deviceID": "customDeviceId",
            "customSessionId": "customSessionId"
        ])
        DZAkamaiTVOSIMACollectorInstance.shared.connect(DZAkamaiTvOsCollectorInstance.shared, ima)
        
        // Connecting with ampfreewheel
        DZAkamaiTVOSFreewheelCollectorInstance.shared.connect(DZAkamaiTvOsCollectorInstance.shared)
        
        ampPlayer?.play(url: self.selectedUrl!)
        
        switch selectedAdProvider {
        case "IMA":
            ima.requestImaAds(adTagUrl: "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpost&cmsid=496&vid=short_onecue&correlator=")
        case "IMA-Skip":
            ima.requestImaAds(adTagUrl: "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dskippablelinear&correlator=")
        case "IMA-Error":
            ima.requestImaAds(adTagUrl: "https://pub1ads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dskippablelinear&correlator=")
        case "Freewheel":
            print("FreeWheel Ads")
            self.ampFreewheel.requestAds(serverUrl: serverUrl, profile: "42015:ios_allinone_profile", siteSectionId: "ios_allinone_demo_site_section", videoAssetId: videoAssetId) {
                adController in
                adController.addPrerollSlot("superpre1")
                adController.addMidrollSlot("supermid1", timePosition: 30)
                adController.addPostrollSlot("postRoll1")
            }
        case "Freewheel-Error":
            self.ampFreewheel.requestAds(serverUrl: "https://demo-bad.v.fwmrm.net", profile: "42015:ios_allinone_profile", siteSectionId: "ios_allinone_demo_site_section", videoAssetId: videoAssetId) {
                adController in
                adController.addPrerollSlot("superpre1")
                adController.addMidrollSlot("supermid1", timePosition: 30)
                adController.addPostrollSlot("postRoll1")
            }
        default:
            break
        }
        
        print("DZ SESSION id \(DZAkamaiTvOsCollectorInstance.shared.getSessionId())")
        print("DZ SESSION View id \(DZAkamaiTvOsCollectorInstance.shared.getSessionViewId())")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            DZAkamaiTvOsCollectorInstance.shared.sendCustomEvent("MyCustomEvent", [
                "customEventMeta1": "Custom_Meta_One"
            ])
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let menuPressRecognizer = UITapGestureRecognizer()
        menuPressRecognizer.addTarget(self, action: #selector(VideoViewController.menuButtonAction(recognizer:)))
        menuPressRecognizer.allowedPressTypes = [NSNumber(value: UIPress.PressType.menu.rawValue)]
        self.view.addGestureRecognizer(menuPressRecognizer)
    }

    @objc func menuButtonAction(recognizer: UITapGestureRecognizer) {
        self.ampPlayer?.stop()
        self.avplayerController?.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
    }
}


extension VideoViewController: PlayerEventObserver {

    func willHandleUrl(_ ampPlayer: AmpPlayer) {
        print("willHandleUrl \(ampPlayer.url)")
    }


//    func onUrlDidSet(_ ampPlayer: AmpPlayer) {
//        print("DATAZOOM: onUrlDidSet")
//    }
//    func onBufferingStateChanged(_ ampPlayer: AmpPlayer) {
//        print("DATAZOOM: onBufferingStateChanged")
//    }
//    func onPlaybackEnded(_ ampPlayer: AmpPlayer) {
//        print("DATAZOOM: onPlaybackEnded")
//    }
//    func onTimeChange(_ ampPlayer: AmpPlayer) {
//        print("DATAZOOM: onTimeChange \(ampPlayer.url)")
//    }

    func willStop(_ ampPlayer: AmpPlayer) {
        print("willStop \(ampPlayer.url)")
    }
}

